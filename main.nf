#! /usr/bin/env nextflow

nextflow.enable.dsl=2

// all image files must be in one place
params.in = "$PWD/image.i.1934-638.taylor.0.restored.fits" // continuum image path
params.out = "$PWD/out" // output path

params.nsubx = 2
params.nsuby = 2
params.runRMSynthesis = false
params.runSNROpt = false

process selavy {
  publishDir "$params.out"

  input:
    path selavyIn_ch
    val image_name
    path inputDir

  output:
    path "selavy.log"
    path "selavy.in"
    path "selavy-SubimageLocations.ann"
    path "*.fits"
    path "*.xml"
    path "*.txt"
    path "PolData"

  script:
    MPI_NPROCS = params.nsubx * params.nsuby + 1
    """
    mkdir PolData

    m4 -DFITS_NAME=${image_name} -DRUN_RMSYNTH=${params.runRMSynthesis} \
    -DNSUBX=${params.nsubx} -DNSUBY=${params.nsuby} \
    -DINPUT_DIR=${inputDir} -DOUT_DIR=${params.out} \
    -DRUN_SNROPT=${params.runSNROpt} ${selavyIn_ch} > selavy.in

    mpiexec -n ${MPI_NPROCS} selavy -c selavy.in > selavy.log
    """

}

process dirname {
  input:
    val file_path

  output:
    stdout

  script:
    """
    dirname ${file_path}
    """
}

workflow {
    fits_file_name = params.in
    // TODO: Make it generic
    fits_parts = fits_file_name.split('\\.')
    fits_name = fits_parts[-5]
    println(fits_name)

    cont_image_path = Channel.fromPath("$params.in")
    inputDir = dirname(cont_image_path)

    selavy_parset_ch = Channel.fromPath("$projectDir/parsets/selavy_template.in")
    selavy(selavy_parset_ch, fits_name, inputDir)
}
