# Nextflow Selavy

This repo contains a Nextflow pipline which performs Selavy source finding using [yandasoft](https://yandasoft.readthedocs.io/en/latest/index.html).

Refer to the [yandasoft](https://yandasoft.readthedocs.io/en/latest/index.html) documentation for information about the processes used.

## Usage

You will need to have yandasoft installed locally or have Docker or Singularity. The yandasoft image can be found in [this url](https://hub.docker.com/r/csirocass/yandasoft) at Dockerhub

```sh
nextflow run nextflow-selavy --in <continuum-image-path>

nextflow run nextflow-selavy --in <continuum-image-path> -with-docker <docker-image>

# To run selavy without rmsynthesis:
nextflow run nextflow-selavy --in <continuum-image-path> --runRMSynthesis=false

# To run selavy with different subdivision
nextflow run nextflow-selavy --in <continuum-image-path> --nsubx=3 --nsuby=3


# Example

nextflow run main.nf --in image.i.1934-638.taylor.0.restored.fits

```
